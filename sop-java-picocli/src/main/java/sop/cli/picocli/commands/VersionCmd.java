// SPDX-FileCopyrightText: 2021 Paul Schaub <vanitasvitae@fsfe.org>
//
// SPDX-License-Identifier: Apache-2.0

package sop.cli.picocli.commands;

import picocli.CommandLine;
import sop.cli.picocli.Print;
import sop.cli.picocli.SopCLI;
import sop.exception.SOPGPException;
import sop.operation.Version;

@CommandLine.Command(name = "version", resourceBundle = "msg_version",
        exitCodeOnInvalidInput = SOPGPException.UnsupportedOption.EXIT_CODE)
public class VersionCmd extends AbstractSopCmd {

    @CommandLine.ArgGroup()
    Exclusive exclusive;

    static class Exclusive {
        @CommandLine.Option(names = "--extended")
        boolean extended;

        @CommandLine.Option(names = "--backend")
        boolean backend;

        @CommandLine.Option(names = "--sop-spec")
        boolean sopSpec;
    }



    @Override
    public void run() {
        Version version = throwIfUnsupportedSubcommand(
                SopCLI.getSop().version(), "version");

        if (exclusive == null) {
            Print.outln(version.getName() + " " + version.getVersion());
            return;
        }

        if (exclusive.extended) {
            Print.outln(version.getExtendedVersion());
            return;
        }

        if (exclusive.backend) {
            Print.outln(version.getBackendVersion());
            return;
        }

        if (exclusive.sopSpec) {
            Print.outln(version.getSopSpecVersion());
            return;
        }
    }
}
