// SPDX-FileCopyrightText: 2023 Paul Schaub <vanitasvitae@fsfe.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * Bindings for SOP subcommands to a SOP binary.
 */
package sop.external.operation;
