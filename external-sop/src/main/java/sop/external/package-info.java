// SPDX-FileCopyrightText: 2023 Paul Schaub <vanitasvitae@fsfe.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * Implementation of sop-java which delegates execution to a binary implementing the SOP command line interface.
 */
package sop.external;
