Source: sop-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Jérôme Charaoui <jerome@riseup.net>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk <!nodoc>,
 default-jdk-doc <!nodoc>,
 default-jdk-headless,
 gradle-debian-helper,
 javahelper,
 junit5 <!nocheck>,
 libjsr305-java,
 libmaven3-core-java,
 libpicocli-java (>> 4),
 maven-repo-helper,
 sqop <!nocheck>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/sop-java.git
Vcs-Browser: https://salsa.debian.org/java-team/sop-java
Homepage: https://github.com/pgpainless/sop-java
Rules-Requires-Root: no

Package: libsop-java-java
Architecture: all
Multi-Arch: foreign
Depends:
 libjsr305-java,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains definitions for a set of Java interfaces describing
 the Stateless OpenPGP Protocol.

Package: libsop-java-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Build-Profiles: <!nodoc>
Depends:
 libjs-jquery,
 libjs-jquery-ui,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java - docs
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains documentation for libsop-java-java.

Package: libsop-java-picocli-java
Architecture: all
Depends:
 libpicocli-java (>> 4),
 libsop-java-java,
 ${misc:Depends},
Multi-Arch: foreign
Description: Stateless OpenPGP Protocol API and CLI for Java - picocli wrapper
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains a wrapper application that transforms the sop-java API
 into a command line application compatible with the SOP-CLI specification.

Package: libsop-java-picocli-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Build-Profiles: <!nodoc>
Depends:
 libjs-jquery,
 libjs-jquery-ui,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java - picocli wrapper docs
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains documentation for libsop-java-picocli-java.

#Package: libexternal-sop-java
#Architecture: all
#Multi-Arch: foreign
#Depends:
# ${misc:Depends},
#Suggests:
# pgpainless-cli,
# sqop,
#Description: Stateless OpenPGP Protocol API and CLI for Java - external backend
# The Stateless OpenPGP Protocol specification defines a generic stateless
# CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
# yet powerful API for the most common OpenPGP related operations.
# .
# This package provides a backend for sop-java that binds to external SOP
# binaries such as sqop, python-sop, etc.

#Package: libexternal-sop-java-doc
#Architecture: all
#Multi-Arch: foreign
#Section: doc
#Depends:
# libjs-jquery,
# libjs-jquery-ui,
# ${misc:Depends},
#Description: Stateless OpenPGP Protocol API and CLI for Java - external-backend
# The Stateless OpenPGP Protocol specification defines a generic stateless
# CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
# yet powerful API for the most common OpenPGP related operations.
# .
# This package contains documentation for libexternal-sop-java.
